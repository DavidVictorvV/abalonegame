var canvasWidth = window.innerWidth;
var canvasHeight = window.innerHeight;
var gameTableItems = [];
var radius;
let pos = [];
var itemsInPos = 0;
var direction = "any";
var selected = 0;
var foundCircle = false;
var teamR;
var paddingTb;
var outW = 0;
var outB = 0;
var subBtn;
var enemyT;
var outColW = 10;
var outRowW = 0;
var outColB = 0;
var outRowB = 5;
var movablePos = [];
var winner = "";
var play = false;
var winnerColor;

function preload() {
  blackSphere = loadImage("images/blackSphere.png");
  whiteSphere = loadImage("images/whiteSphere.png");
}

function startGame() {
  play = true;
  winner = "";
  startBtn.hide();
  defaultTeamPos();
}

function restartGame() {
  play = true;
  winner = "";
  replayBtn.hide();
  defaultTeamPos();
}

function setup() {
  startBtn = createButton("START");
  startBtn.position(canvasWidth / 2 - 30, canvasHeight / 2);
  startBtn.mousePressed(startGame);

  replayBtn = createButton("REPLAY");
  replayBtn.position(canvasWidth / 2 - 30, canvasHeight / 2);
  replayBtn.mousePressed(restartGame);
  replayBtn.hide();

  createCanvas(canvasWidth, canvasHeight);
  if (canvasWidth < 400 || 400 > canvasHeight) {
    radius = 15;
    paddingTb = 7;
  } else if (canvasWidth < canvasHeight) {
    paddingTb = 0;
    radius = floor(canvasWidth / 10) - 30;
  } else if (canvasWidth > canvasHeight) {
    paddingTb = -20;
    radius = floor(canvasHeight / 10) - 30;
  }
  gameTable();
  defaultTeamPos();
  resetButton();
  playerNamesInput();
  botButtons();
}

function draw() {
  textAlign(CENTER);
  background(20, 150, 130);
  playerNames();
  gameTableBG();
  checkAndDrawColors();
  if (play) {
    if (teamR == botTeam) {
      startBot();
    }
    if (outRowW == 3) {
      if (pName1.length == 0) {
        winner = "BLACK";
      } else {
        winner = pName2;
      }
      winnerColor = "black";
      play = false;
    } else if (outRowB - 5 == 3) {
      if (pName1.length == 0) {
        winner = "WHITE";
      } else {
        winner = pName1;
      }
      winnerColor = "white";
      play = false;
    }
  }
  if (winner != "") {
    fill(winnerColor);
    textSize(100);
    text(`${winner} WINS!!!`, canvasWidth / 2, canvasHeight / 2);
    replayBtn.show();
    stopBot();
  }
}

function mouseClicked() {
  if (play) {
    for (let column = 0; column < gameTableItems.length - 1; column++) {
      for (let tLine = 0; tLine < gameTableItems[column].length - 1; tLine++) {
        if (checkForCircle(column, tLine)) {
          if (
            gameTableItems[column][tLine].team == "MOVE1" ||
            gameTableItems[column][tLine].team == "MoveLR"
          ) {
            moveB(column, tLine);
            resetAllDif();
          }

          if (gameTableItems[column][tLine].team == "MoveLeftMultiple2") {
            moveBMultipleLeft2(column, tLine);
            resetAllDif();
          }
          if (gameTableItems[column][tLine].team == "MoveRigthMultiple2") {
            moveBMultipleRigth2(column, tLine);
            resetAllDif();
          }
          if (gameTableItems[column][tLine].team == "MoveLeftMultiple3") {
            moveBMultipleLeft3(column, tLine);
            resetAllDif();
          }
          if (gameTableItems[column][tLine].team == "MoveRigthMultiple3") {
            moveBMultipleRigth3(column, tLine);
            resetAllDif();
          }
          if (
            gameTableItems[column][tLine].team == "MoveDiagonalLR-LU2" ||
            gameTableItems[column][tLine].team == "MoveDiagonalLR-RU2"
          ) {
            moveBMultipleDiagonalUp2(column, tLine);
            resetAllDif();
          }
          if (
            gameTableItems[column][tLine].team == "MoveDiagonalLR-LD2" ||
            gameTableItems[column][tLine].team == "MoveDiagonalLR-RD2"
          ) {
            moveBMultipleDiagonalDown2(column, tLine);
            resetAllDif();
          }
          if (
            gameTableItems[column][tLine].team == "MoveDiagonalLR-LU3" ||
            gameTableItems[column][tLine].team == "MoveDiagonalLR-RU3"
          ) {
            moveBMultipleDiagonalLeftUp3(column, tLine);
            resetAllDif();
          }
          if (
            gameTableItems[column][tLine].team == "MoveDiagonalLR-LD3" ||
            gameTableItems[column][tLine].team == "MoveDiagonalLR-RD3"
          ) {
            moveBMultipleDiagonalLeftDown3(column, tLine);
            resetAllDif();
          }
          if (
            gameTableItems[column][tLine].team == "MoveDiagonalRL-LU2" ||
            gameTableItems[column][tLine].team == "MoveDiagonalRL-RU2"
          ) {
            moveBMultipleDiagonalUp2RL(column, tLine);
            resetAllDif();
          }
          if (
            gameTableItems[column][tLine].team == "MoveDiagonalRL-LD2" ||
            gameTableItems[column][tLine].team == "MoveDiagonalRL-RD2"
          ) {
            moveBMultipleDiagonalDown2RL(column, tLine);
            resetAllDif();
          }
          if (
            gameTableItems[column][tLine].team == "MoveDiagonalRL-LU3" ||
            gameTableItems[column][tLine].team == "MoveDiagonalRL-RU3"
          ) {
            moveBMultipleDiagonalLeftUp3RL(column, tLine);
            resetAllDif();
          }
          if (
            gameTableItems[column][tLine].team == "MoveDiagonalRL-LD3" ||
            gameTableItems[column][tLine].team == "MoveDiagonalRL-RD3"
          ) {
            moveBMultipleDiagonalLeftDown3RL(column, tLine);
            resetAllDif();
          }
          if (gameTableItems[column][tLine].team == "MoveRLD") {
            moveBPushRLD(column, tLine);
            resetAllDif();
          }
          if (gameTableItems[column][tLine].team == "MoveLRU") {
            moveBPushLRU(column, tLine);
            resetAllDif();
          }
          if (gameTableItems[column][tLine].team == "MoveRLU") {
            moveBPushRLU(column, tLine);
            resetAllDif();
          }
          if (gameTableItems[column][tLine].team == "MoveLRD") {
            moveBPushLRD(column, tLine);
            resetAllDif();
          }
          if (gameTableItems[column][tLine].team == "MoveR") {
            moveBPushR(column, tLine);
            resetAllDif();
          }
          if (gameTableItems[column][tLine].team == "MoveL") {
            moveBPushL(column, tLine);
            resetAllDif();
          }
          if (gameTableItems[column][tLine].team == teamR) {
            pos = [];
            itemsInPos = 0;
            pos[itemsInPos] = new Array();
            pos[itemsInPos].push(column);
            pos[itemsInPos].push(tLine);
            itemsInPos++;
            selectPiece();
          }
        }
      }
    }
  }
}

function checkForCircle(column, tLine) {
  if (
    mouseX >= gameTableItems[column][tLine].x - radius / 2 &&
    mouseX <= gameTableItems[column][tLine].x + radius / 2 &&
    mouseY >= gameTableItems[column][tLine].y - radius / 2 &&
    mouseY <= gameTableItems[column][tLine].y + radius / 2
  ) {
    return true;
  }
  return false;
}

function resetAllSelected() {
  resetMove();
  for (let column = 0; column < gameTableItems.length; column++) {
    for (let tLine = 0; tLine < gameTableItems[column].length; tLine++) {
      if (gameTableItems[column][tLine].team == "SELECTED") {
        gameTableItems[column][tLine].setTeam(teamR);
      }
    }
  }
  selected = 0;
}

function resetMove() {
  for (let column = 0; column < gameTableItems.length; column++) {
    for (let tLine = 0; tLine < gameTableItems[column].length; tLine++) {
      if (
        gameTableItems[column][tLine].team == "MoveRLD" ||
        gameTableItems[column][tLine].team == "MoveLRU" ||
        gameTableItems[column][tLine].team == "MoveLRD" ||
        gameTableItems[column][tLine].team == "MoveRLU" ||
        gameTableItems[column][tLine].team == "MoveL" ||
        gameTableItems[column][tLine].team == "MoveR"
      ) {
        gameTableItems[column][tLine].setTeam(enemyT);
      }
      if (
        gameTableItems[column][tLine].team != "NO" &&
        gameTableItems[column][tLine].team != "SELECTED" &&
        gameTableItems[column][tLine].team != "BLACK" &&
        gameTableItems[column][tLine].team != "WHITE" &&
        gameTableItems[column][tLine].team != "OUT" &&
        gameTableItems[column][tLine].team != "OUTW" &&
        gameTableItems[column][tLine].team != "OUTB"
      ) {
        gameTableItems[column][tLine].setTeam("EMPTY");
      }
    }
  }
}

function selectPiece() {
  resetMove();
  resetAllSelected();

  gameTableItems[pos[itemsInPos - 1][0]][pos[itemsInPos - 1][1]].setTeam(
    "SELECTED"
  );
  movablePos = [];
  showMovable1();
  showMovable2();
}

function showMovable1() {
  columnPos = pos[itemsInPos - 1][0];
  defColumnPos = pos[itemsInPos - 1][0];
  rowPos = pos[itemsInPos - 1][1];

  for (let column = columnPos - 1; column <= columnPos + 1; column++) {
    for (let tLine = rowPos - 1; tLine <= rowPos; tLine++) {
      if (defColumnPos != 0) {
        if (
          column == defColumnPos - 1 &&
          gameTableItems[column][tLine + 1].team == "EMPTY"
        ) {
          gameTableItems[column][tLine + 1].setTeam("MOVE1");
          var obj = {
            col: column,
            row: tLine + 1,
          };
          movablePos.push(obj);
        }
      }
      if (defColumnPos != 9) {
        if (
          column == defColumnPos + 1 &&
          gameTableItems[column][tLine].team == "EMPTY"
        ) {
          var obj = {
            col: column,
            row: tLine,
          };
          movablePos.push(obj);
          gameTableItems[column][tLine].setTeam("MOVE1");
        }
      }
    }
  }

  for (
    let column = pos[itemsInPos - 1][1] - 1;
    column <= pos[itemsInPos - 1][1] + 1;
    column++
  ) {
    if (column >= 0 && column < gameTableItems[pos[itemsInPos - 1][0]].length) {
      if (gameTableItems[pos[itemsInPos - 1][0]][column].team == "EMPTY") {
        var obj = {
          col: pos[itemsInPos - 1][0],
          row: column,
        };
        movablePos.push(obj);
        gameTableItems[pos[itemsInPos - 1][0]][column].setTeam("MOVE1");
      }
    }
  }
}

function showMovable2() {
  let found = false;
  let found2 = false;
  let col = pos[itemsInPos - 1][0];
  let row = pos[itemsInPos - 1][1];

  if (teamR == "BLACK") {
    enemyT = "WHITE";
  } else {
    enemyT = "BLACK";
  }

  if (col > 2) {
    //up left
    for (let i = 1; i < 3; i++) {
      if (
        gameTableItems[col - i][row].team == teamR &&
        gameTableItems[col - i - 1][row].team == "EMPTY"
      ) {
        var obj = {
          col: col - i - 1,
          row: row,
        };
        movablePos.push(obj);
        if (i == 2 && gameTableItems[col - i + 1][row].team == teamR) {
          gameTableItems[col - i - 1][row].setTeam("MoveLR");
        } else if (i != 2) {
          gameTableItems[col - i - 1][row].setTeam("MoveLR");
        }
      } else if (
        gameTableItems[col - i][row].team == teamR &&
        gameTableItems[col - 1][row].team == teamR &&
        gameTableItems[col - i - 1][row].team == enemyT
      ) {
        var obj = {
          col: col - i - 1,
          row: row,
        };
        movablePos.push(obj);
        if (
          i != 2 &&
          gameTableItems[col - i - 2][row].team != enemyT &&
          gameTableItems[col - i - 2][row].team != teamR
        ) {
          gameTableItems[col - i - 1][row].setTeam("MoveRLU");
        } else if (i == 2 && col - i - 3 > 0 && row > 0) {
          if (
            gameTableItems[col - i - 2][row].team != enemyT &&
            gameTableItems[col - i - 2][row].team != teamR
          ) {
            gameTableItems[col - i - 1][row].setTeam("MoveRLU");
          }

          if (
            gameTableItems[col - i - 3][row].team != enemyT &&
            gameTableItems[col - i - 3][row].team != teamR
          ) {
            gameTableItems[col - i - 1][row].setTeam("MoveRLU");
          }
        } else if (i == 2) {
          gameTableItems[col - i - 1][row].setTeam("MoveRLU");
        }
      }
    }
  }
  //down right
  if (col < 8) {
    for (let i = 1; i < 3; i++) {
      if (
        gameTableItems[col + i][row].team == teamR &&
        gameTableItems[col + i + 1][row].team == "EMPTY"
      ) {
        var obj = {
          col: col + i + 1,
          row: row,
        };
        movablePos.push(obj);
        if (i == 2 && gameTableItems[col + i - 1][row].team == teamR) {
          gameTableItems[col + i + 1][row].setTeam("MoveLR");
        } else if (i != 2) {
          gameTableItems[col + i + 1][row].setTeam("MoveLR");
        }
      } else if (
        gameTableItems[col + i][row].team == teamR &&
        gameTableItems[col + 1][row].team == teamR &&
        gameTableItems[col + i + 1][row].team == enemyT
      ) {
        var obj = {
          col: col + i + 1,
          row: row,
        };
        movablePos.push(obj);
        if (
          i != 2 &&
          gameTableItems[col + i + 2][row].team != enemyT &&
          gameTableItems[col + i + 2][row].team != teamR
        ) {
          gameTableItems[col + i + 1][row].setTeam("MoveLRD");
        } else if (i == 2 && col + i + 2 < 9) {
          if (
            gameTableItems[col + i + 2][row].team != teamR &&
            gameTableItems[col + i + 2][row].team != enemyT
          ) {
            gameTableItems[col + i + 1][row].setTeam("MoveLRD");
          }
          if (
            gameTableItems[col + i + 3][row].team != enemyT &&
            gameTableItems[col + i + 3][row].team != teamR
          ) {
            gameTableItems[col + i + 1][row].setTeam("MoveLRD");
          }
        } else if (i == 2) {
          gameTableItems[col + i + 1][row].setTeam("MoveLRD");
        }
      }
    }
  }

  //up right
  if (col > 2 && row < 8) {
    for (let i = 1; i < 3; i++) {
      if (
        gameTableItems[col - i][row + i].team == teamR &&
        gameTableItems[col - i - 1][row + i + 1].team == "EMPTY"
      ) {
        var obj = {
          col: col - i - 1,
          row: row + i + 1,
        };
        movablePos.push(obj);
        if (i == 2 && gameTableItems[col - i + 1][row + i - 1].team == teamR) {
          gameTableItems[col - i - 1][row + i + 1].setTeam("MoveLR");
        } else if (i != 2) {
          gameTableItems[col - i - 1][row + i + 1].setTeam("MoveLR");
        }
      } else if (
        gameTableItems[col - i][row + i].team == teamR &&
        gameTableItems[col - 1][row + 1].team == teamR &&
        gameTableItems[col - i - 1][row + i + 1].team == enemyT
      ) {
        if (
          i != 2 &&
          gameTableItems[col - i - 2][row + i + 2].team != enemyT &&
          gameTableItems[col - i - 2][row + i + 2].team != teamR
        ) {
          var obj = {
            col: col - i - 1,
            row: row + i + 1,
          };
          movablePos.push(obj);
          gameTableItems[col - i - 1][row + i + 1].setTeam("MoveLRU");
        } else if (i == 2 && col - i - 3 > 0 && row > 0) {
          if (
            gameTableItems[col - i - 2][row + i + 2].team != teamR &&
            gameTableItems[col - i - 2][row + i + 2].team != enemyT
          ) {
            var obj = {
              col: col - i - 1,
              row: row + i + 1,
            };
            movablePos.push(obj);
            gameTableItems[col - i - 1][row + i + 1].setTeam("MoveLRU");
          }
          if (
            gameTableItems[col - i - 3][row + i + 3].team != enemyT &&
            gameTableItems[col - i - 3][row + i + 3].team != teamR
          ) {
            var obj = {
              col: col - i - 1,
              row: row + i + 1,
            };
            movablePos.push(obj);
            gameTableItems[col - i - 1][row + i + 1].setTeam("MoveLRU");
          }
        } else if (i == 2) {
          var obj = {
            col: col - i - 1,
            row: row + i + 1,
          };
          movablePos.push(obj);
          gameTableItems[col - i - 1][row + i + 1].setTeam("MoveLRU");
        }
      }
    }
  }

  // down left
  if (row > 2 && col < 9) {
    for (let i = 1; i < 3; i++) {
      if (
        gameTableItems[col + i][row - i].team == teamR &&
        gameTableItems[col + i + 1][row - i - 1].team == "EMPTY"
      ) {
        var obj = {
          col: col + i + 1,
          row: row - i - 1,
        };
        movablePos.push(obj);
        if (i == 2 && gameTableItems[col + i - 1][row - i + 1].team == teamR) {
          gameTableItems[col + i + 1][row - i - 1].setTeam("MoveLR");
        } else if (i != 2) {
          gameTableItems[col + i + 1][row - i - 1].setTeam("MoveLR");
        }
      } else if (
        gameTableItems[col + i][row - i].team == teamR &&
        gameTableItems[col + 1][row - 1].team == teamR &&
        gameTableItems[col + i + 1][row - i - 1].team == enemyT
      ) {
        if (
          i != 2 &&
          gameTableItems[col + i + 2][row - i - 2].team != enemyT &&
          gameTableItems[col + i + 2][row - i - 2].team != teamR
        ) {
          gameTableItems[col + i + 1][row - i - 1].setTeam("MoveRLD");
        } else if (i == 2 && row - i - 3 >= 0) {
          if (
            gameTableItems[col + i + 2][row - i - 2].team != teamR &&
            gameTableItems[col + i + 2][row - i - 2].team != enemyT
          ) {
            var obj = {
              col: col + i + 1,
              row: row - i - 1,
            };
            movablePos.push(obj);
            gameTableItems[col + i + 1][row - i - 1].setTeam("MoveRLD");
          } else if (
            gameTableItems[col + i + 3][row - i - 3].team != enemyT &&
            gameTableItems[col + i + 3][row - i - 3].team != teamR
          ) {
            var obj = {
              col: col + i + 1,
              row: row - i - 1,
            };
            movablePos.push(obj);
            gameTableItems[col + i + 1][row - i - 1].setTeam("MoveRLD");
          }
        } else if (i == 2) {
          var obj = {
            col: col + i + 1,
            row: row - i - 1,
          };
          movablePos.push(obj);
          gameTableItems[col + i + 1][row - i - 1].setTeam("MoveRLD");
        }
      }
    }
  }
  //right
  if (row < 8 && row > 0) {
    for (let i = 1; i < 3; i++) {
      if (
        gameTableItems[col][row + i + 1].team == "EMPTY" &&
        gameTableItems[col][row + i].team == teamR
      ) {
        var obj = {
          col: col,
          row: row + i + 1,
        };
        movablePos.push(obj);
        if (i == 2 && gameTableItems[col][row + i - 1].team == teamR) {
          gameTableItems[col][row + i + 1].setTeam("MoveLR");
        } else if (i != 2) {
          gameTableItems[col][row + i + 1].setTeam("MoveLR");
        }
      } else if (
        gameTableItems[col][row + i].team == teamR &&
        gameTableItems[col][row + 1].team == teamR &&
        gameTableItems[col][row + i + 1].team == enemyT
      ) {
        var obj = {
          col: col,
          row: row + i + 1,
        };
        movablePos.push(obj);
        if (
          i != 2 &&
          gameTableItems[col][row + i + 2].team != enemyT &&
          gameTableItems[col][row + i + 2].team != teamR
        ) {
          gameTableItems[col][row + i + 1].setTeam("MoveR");
        } else if (i == 2 && row + i + 3 <= 10) {
          if (
            gameTableItems[col][row + i + 2].team != teamR &&
            gameTableItems[col][row + i + 2].team != enemyT
          ) {
            gameTableItems[col][row + i + 1].setTeam("MoveR");
          } else if (
            gameTableItems[col][row + i + 3].team != teamR &&
            gameTableItems[col][row + i + 3].team != enemyT
          ) {
            if (
              gameTableItems[col][row + i + 2].team != teamR &&
              gameTableItems[col][row + i + 2].team != enemyT
            ) {
              gameTableItems[col][row + i + 1].setTeam("MoveR");
            }
          }
        } else if (i == 2) {
          gameTableItems[col][row + i + 1].setTeam("MoveR");
        }
      }
    }
  }
  //left
  if (row < 10 && row > 2) {
    for (let i = 1; i < 3; i++) {
      if (
        gameTableItems[col][row - i - 1].team == "EMPTY" &&
        gameTableItems[col][row - i].team == teamR
      ) {
        var obj = {
          col: col,
          row: row - i - 1,
        };
        movablePos.push(obj);
        if (i == 2 && gameTableItems[col][row - i + 1].team == teamR) {
          gameTableItems[col][row - i - 1].setTeam("MoveLR");
        } else if (i != 2) {
          gameTableItems[col][row - i - 1].setTeam("MoveLR");
        }
      } else if (
        gameTableItems[col][row - i].team == teamR &&
        gameTableItems[col][row - 1].team == teamR &&
        gameTableItems[col][row - i - 1].team == enemyT
      ) {
        var obj = {
          col: col,
          row: row - i - 1,
        };
        movablePos.push(obj);
        if (
          i != 2 &&
          gameTableItems[col][row - i - 2].team != enemyT &&
          gameTableItems[col][row - i - 2].team != teamR
        ) {
          gameTableItems[col][row - i - 1].setTeam("MoveL");
        } else if (i == 2 && row - i - 3 > 0) {
          if (
            gameTableItems[col][row - i - 2].team != teamR &&
            gameTableItems[col][row - i - 2].team != enemyT
          ) {
            gameTableItems[col][row - i - 1].setTeam("MoveL");
          }
          if (
            gameTableItems[col][row - i - 3].team != enemyT &&
            gameTableItems[col][row - i - 3].team != teamR
          ) {
            if (
              gameTableItems[col][row - i - 2].team != teamR &&
              gameTableItems[col][row - i - 2].team != enemyT
            ) {
              gameTableItems[col][row - i - 1].setTeam("MoveL");
            }
          }
        } else if (i == 2) {
          gameTableItems[col][row - i - 1].setTeam("MoveL");
        }
      }
    }
  }

  //left down multiple(2)
  if (
    gameTableItems[col][row - 1].team == teamR &&
    gameTableItems[col + 1][row - 2].team == "EMPTY" &&
    gameTableItems[col + 1][row - 1].team == "MOVE1"
  ) {
    var obj = {
      col: col + 1,
      row: row - 2,
    };
    movablePos.push(obj);
    gameTableItems[col + 1][row - 2].setTeam("MoveLeftMultiple2");
  }
  //left down multiple(3)
  if (row > 2) {
    if (
      gameTableItems[col][row - 2].team == teamR &&
      gameTableItems[col + 1][row - 3].team == "EMPTY" &&
      gameTableItems[col + 1][row - 2].team == "MoveLeftMultiple2"
    ) {
      var obj = {
        col: col + 1,
        row: row - 3,
      };
      movablePos.push(obj);
      gameTableItems[col + 1][row - 3].setTeam("MoveLeftMultiple3");
    }
  }
  //rigth down multiple(2)
  if (
    gameTableItems[col][row + 1].team == teamR &&
    gameTableItems[col + 1][row + 1].team == "EMPTY" &&
    gameTableItems[col + 1][row].team == "MOVE1"
  ) {
    var obj = {
      col: col + 1,
      row: row + 1,
    };
    movablePos.push(obj);
    gameTableItems[col + 1][row + 1].setTeam("MoveRigthMultiple2");
  }
  //rigth down multiple(3)
  if (row < 9) {
    if (
      gameTableItems[col][row + 2].team == teamR &&
      gameTableItems[col + 1][row + 2].team == "EMPTY" &&
      gameTableItems[col + 1][row + 1].team == "MoveRigthMultiple2"
    ) {
      var obj = {
        col: col + 1,
        row: row + 2,
      };
      movablePos.push(obj);
      gameTableItems[col + 1][row + 2].setTeam("MoveRigthMultiple3");
    }
  }
  //left up multiple(2)
  if (
    gameTableItems[col][row - 1].team == teamR &&
    gameTableItems[col - 1][row - 1].team == "EMPTY" &&
    gameTableItems[col - 1][row].team == "MOVE1"
  ) {
    var obj = {
      col: col - 1,
      row: row - 1,
    };
    movablePos.push(obj);
    gameTableItems[col - 1][row - 1].setTeam("MoveLeftMultiple2");
  }
  //left up multiple(3)
  if (row > 2) {
    if (
      gameTableItems[col][row - 2].team == teamR &&
      gameTableItems[col - 1][row - 2].team == "EMPTY" &&
      gameTableItems[col - 1][row - 1].team == "MoveLeftMultiple2"
    ) {
      var obj = {
        col: col - 1,
        row: row - 2,
      };
      movablePos.push(obj);
      gameTableItems[col - 1][row - 2].setTeam("MoveLeftMultiple3");
    }
  }
  //rigth up multiple(2)
  if (
    gameTableItems[col][row + 1].team == teamR &&
    gameTableItems[col - 1][row + 2].team == "EMPTY" &&
    gameTableItems[col - 1][row + 1].team == "MOVE1"
  ) {
    var obj = {
      col: col - 1,
      row: row + 2,
    };
    movablePos.push(obj);
    gameTableItems[col - 1][row + 2].setTeam("MoveRigthMultiple2");
  }
  //rigth up multiple(3)
  if (row < 9) {
    if (
      gameTableItems[col][row + 2].team == teamR &&
      gameTableItems[col - 1][row + 3].team == "EMPTY" &&
      gameTableItems[col - 1][row + 2].team == "MoveRigthMultiple2"
    ) {
      var obj = {
        col: col - 1,
        row: row + 3,
      };
      movablePos.push(obj);
      gameTableItems[col - 1][row + 3].setTeam("MoveRigthMultiple3");
    }
  }

  //diagonal l-r move l-u(2)
  if (
    gameTableItems[col - 1][row + 1].team == teamR &&
    gameTableItems[col - 2][row + 1].team == "EMPTY" &&
    gameTableItems[col - 1][row].team == "MOVE1"
  ) {
    var obj = {
      col: col - 2,
      row: row + 1,
    };
    movablePos.push(obj);
    gameTableItems[col - 2][row + 1].setTeam("MoveDiagonalLR-LU2");
  }
  //diagonal r-l move l-u(2)
  if (
    gameTableItems[col - 1][row].team == teamR &&
    gameTableItems[col - 1][row - 1].team == "EMPTY" &&
    gameTableItems[col][row - 1].team == "MOVE1"
  ) {
    var obj = {
      col: col - 1,
      row: row - 1,
    };
    movablePos.push(obj);
    gameTableItems[col - 1][row - 1].setTeam("MoveDiagonalRL-LU2");
  }
  //diagonal l-r move l-u(3)
  if (col > 2 && row < 9) {
    if (
      gameTableItems[col - 2][row + 2].team == teamR &&
      gameTableItems[col - 3][row + 2].team == "EMPTY" &&
      gameTableItems[col - 2][row + 1].team == "MoveDiagonalLR-LU2"
    ) {
      var obj = {
        col: col - 3,
        row: row + 2,
      };
      movablePos.push(obj);
      gameTableItems[col - 3][row + 2].setTeam("MoveDiagonalLR-LU3");
    }
  }
  //diagonal r-l move l-u(3)
  if (col > 2 && row < 9) {
    if (
      gameTableItems[col - 2][row].team == teamR &&
      gameTableItems[col - 2][row - 1].team == "EMPTY" &&
      gameTableItems[col - 1][row - 1].team == "MoveDiagonalRL-LU2"
    ) {
      var obj = {
        col: col - 2,
        row: row - 1,
      };
      movablePos.push(obj);
      gameTableItems[col - 2][row - 1].setTeam("MoveDiagonalRL-LU3");
    }
  }
  //diagonal l-r move l-d(2)
  if (
    gameTableItems[col + 1][row - 1].team == teamR &&
    gameTableItems[col + 1][row - 2].team == "EMPTY" &&
    gameTableItems[col][row - 1].team == "MOVE1"
  ) {
    var obj = {
      col: col + 1,
      row: row - 2,
    };
    movablePos.push(obj);
    gameTableItems[col + 1][row - 2].setTeam("MoveDiagonalLR-LD2");
  }
  //diagonal r-l move l-d(2)
  if (
    gameTableItems[col + 1][row].team == teamR &&
    gameTableItems[col + 2][row - 1].team == "EMPTY" &&
    gameTableItems[col + 1][row - 1].team == "MOVE1"
  ) {
    var obj = {
      col: col + 2,
      row: row - 1,
    };
    movablePos.push(obj);
    gameTableItems[col + 2][row - 1].setTeam("MoveDiagonalRL-LD2");
  }
  //diagonal l-r move l-d(3)
  if (col < 9 && row > 2)
    if (
      gameTableItems[col + 2][row - 2].team == teamR &&
      gameTableItems[col + 2][row - 3].team == "EMPTY" &&
      gameTableItems[col + 1][row - 2].team == "MoveDiagonalLR-LD2"
    ) {
      var obj = {
        col: col + 2,
        row: row - 3,
      };
      movablePos.push(obj);
      gameTableItems[col + 2][row - 3].setTeam("MoveDiagonalLR-LD3");
    }
  //diagonal r-l move l-d(3)
  if (col < 8) {
    if (
      gameTableItems[col + 2][row].team == teamR &&
      gameTableItems[col + 3][row - 1].team == "EMPTY" &&
      gameTableItems[col + 2][row - 1].team == "MoveDiagonalRL-LD2"
    ) {
      var obj = {
        col: col + 3,
        row: row - 1,
      };
      movablePos.push(obj);
      gameTableItems[col + 3][row - 1].setTeam("MoveDiagonalRL-LD3");
    }
  }
  //diagonal l-r move r-d(2)
  if (col < 9) {
    if (
      gameTableItems[col + 1][row - 1].team == teamR &&
      gameTableItems[col + 2][row - 1].team == "EMPTY" &&
      gameTableItems[col + 1][row].team == "MOVE1"
    ) {
      var obj = {
        col: col + 2,
        row: row - 1,
      };
      movablePos.push(obj);
      gameTableItems[col + 2][row - 1].setTeam("MoveDiagonalLR-RD2");
    }
  }
  //diagonal r-l move r-d(2)
  if (col < 7) {
    if (
      gameTableItems[col + 1][row].team == teamR &&
      gameTableItems[col + 1][row + 1].team == "EMPTY" &&
      gameTableItems[col][row + 1].team == "MOVE1"
    ) {
      var obj = {
        col: col + 1,
        row: row + 1,
      };
      movablePos.push(obj);
      gameTableItems[col + 1][row + 1].setTeam("MoveDiagonalRL-RD2");
    }
  }
  //diagonal l-r move r-d(3)
  if (col < 8 && row > 2) {
    if (
      gameTableItems[col + 2][row - 2].team == teamR &&
      gameTableItems[col + 3][row - 2].team == "EMPTY" &&
      gameTableItems[col + 2][row - 1].team == "MoveDiagonalLR-RD2"
    ) {
      var obj = {
        col: col + 3,
        row: row - 2,
      };
      movablePos.push(obj);
      gameTableItems[col + 3][row - 2].setTeam("MoveDiagonalLR-RD3");
    }
  }

  //diagonal r-l move r-d(3)
  if (col < 9 && col > 1 && row > 1 && row < 10) {
    if (
      gameTableItems[col + 2][row].team == teamR &&
      gameTableItems[col + 2][row + 1].team == "EMPTY" &&
      gameTableItems[col + 1][row + 1].team == "MoveDiagonalRL-RD2"
    ) {
      var obj = {
        col: col + 2,
        row: row + 1,
      };
      movablePos.push(obj);
      gameTableItems[col + 2][row + 1].setTeam("MoveDiagonalRL-RD3");
    }
  }

  //diagonal l-r move r-u(2)
  if (
    gameTableItems[col - 1][row + 1].team == teamR &&
    gameTableItems[col - 1][row + 2].team == "EMPTY" &&
    gameTableItems[col][row + 1].team == "MOVE1"
  ) {
    var obj = {
      col: col - 1,
      row: row + 2,
    };
    movablePos.push(obj);
    gameTableItems[col - 1][row + 2].setTeam("MoveDiagonalLR-RU2");
  }
  //diagonal r-l move r-u(2)
  if (
    gameTableItems[col - 1][row].team == teamR &&
    gameTableItems[col - 2][row + 1].team == "EMPTY" &&
    gameTableItems[col - 1][row + 1].team == "MOVE1"
  ) {
    var obj = {
      col: col - 2,
      row: row + 1,
    };
    movablePos.push(obj);
    gameTableItems[col - 2][row + 1].setTeam("MoveDiagonalRL-RU2");
  }
  //diagonal l-r move r-u(3)
  if (col > 2 && row > 0 && row < 9)
    if (
      gameTableItems[col - 2][row + 2].team == teamR &&
      gameTableItems[col - 2][row + 3].team == "EMPTY" &&
      gameTableItems[col - 1][row + 2].team == "MoveDiagonalLR-RU2"
    ) {
      var obj = {
        col: col - 2,
        row: row + 3,
      };
      movablePos.push(obj);
      gameTableItems[col - 2][row + 3].setTeam("MoveDiagonalLR-RU3");
    }
  //diagonal r-l move r-u(3)
  if (col > 2 && row > 2 && row < 9)
    if (
      gameTableItems[col - 2][row].team == teamR &&
      gameTableItems[col - 3][row + 1].team == "EMPTY" &&
      gameTableItems[col - 2][row + 1].team == "MoveDiagonalRL-RU2"
    ) {
      var obj = {
        col: col - 3,
        row: row + 1,
      };
      movablePos.push(obj);
      gameTableItems[col - 3][row + 1].setTeam("MoveDiagonalRL-RU3");
    }
}

function checkAndDrawColors() {
  gameTableItems.forEach((row) => {
    row.forEach((piece) => {
      piece.drawPiece();
    });
  });
  gameTableItems.forEach((row) => {
    row.forEach((piece) => {
      piece.checkColor();
    });
  });
}

function moveB(column, tLine) {
  gameTableItems[column][tLine].setTeam(teamR);
  gameTableItems[pos[itemsInPos - 1][0]][pos[itemsInPos - 1][1]].setTeam(
    "EMPTY"
  );
  changeTeam();
}

function moveBPushRLD(column, tLine) {
  gameTableItems[column][tLine].setTeam(teamR);

  if (gameTableItems[column + 1][tLine - 1].team == "EMPTY") {
    gameTableItems[column + 1][tLine - 1].setTeam(enemyT);
  } else if (
    gameTableItems[column + 1][tLine - 1].team == "OUT" ||
    gameTableItems[column + 1][tLine - 1].team == "OUTW" ||
    gameTableItems[column + 1][tLine - 1].team == "OUTB"
  ) {
    if (teamR == "BLACK") {
      gameTableItems[outColW][outRowW].setTeam("OUTW");
      outRowW++;
    } else {
      gameTableItems[outColB][outRowB].setTeam("OUTB");
      outRowB++;
    }
  } else if (gameTableItems[column + 1][tLine - 1].team == enemyT) {
    if (gameTableItems[column + 2][tLine - 2].team == "EMPTY") {
      gameTableItems[column + 2][tLine - 2].setTeam(enemyT);
    } else {
      if (teamR == "BLACK") {
        gameTableItems[outColW][outRowW].setTeam("OUTW");
        outRowW++;
      } else {
        gameTableItems[outColB][outRowB].setTeam("OUTB");
        outRowB++;
      }
    }
  }

  gameTableItems[pos[itemsInPos - 1][0]][pos[itemsInPos - 1][1]].setTeam(
    "EMPTY"
  );
  changeTeam();
}

function moveBPushLRD(column, tLine) {
  gameTableItems[column][tLine].setTeam(teamR);

  if (gameTableItems[column + 1][tLine].team == "EMPTY") {
    gameTableItems[column + 1][tLine].setTeam(enemyT);
  } else if (
    gameTableItems[column + 1][tLine].team == "OUT" ||
    gameTableItems[column + 1][tLine].team == "OUTW" ||
    gameTableItems[column + 1][tLine].team == "OUTB"
  ) {
    if (teamR == "BLACK") {
      gameTableItems[outColW][outRowW].setTeam("OUTW");
      outRowW++;
    } else {
      gameTableItems[outColB][outRowB].setTeam("OUTB");
      outRowB++;
    }
  } else if (gameTableItems[column + 1][tLine].team == enemyT) {
    if (gameTableItems[column + 2][tLine].team == "EMPTY") {
      gameTableItems[column + 2][tLine].setTeam(enemyT);
    } else {
      if (teamR == "BLACK") {
        gameTableItems[outColW][outRowW].setTeam("OUTW");
        outRowW++;
      } else {
        gameTableItems[outColB][outRowB].setTeam("OUTB");
        outRowB++;
      }
    }
  }

  gameTableItems[pos[itemsInPos - 1][0]][pos[itemsInPos - 1][1]].setTeam(
    "EMPTY"
  );
  changeTeam();
}

function moveBPushR(column, tLine) {
  gameTableItems[column][tLine].setTeam(teamR);

  if (gameTableItems[column][tLine + 1].team == "EMPTY") {
    gameTableItems[column][tLine + 1].setTeam(enemyT);
  } else if (
    gameTableItems[column][tLine + 1].team == "OUT" ||
    gameTableItems[column][tLine + 1].team == "OUTW" ||
    gameTableItems[column][tLine + 1].team == "OUTB"
  ) {
    if (teamR == "BLACK") {
      gameTableItems[outColW][outRowW].setTeam("OUTW");
      outRowW++;
    } else {
      gameTableItems[outColB][outRowB].setTeam("OUTB");
      outRowB++;
    }
  } else if (gameTableItems[column][tLine + 1].team == enemyT) {
    if (gameTableItems[column][tLine + 2].team == "EMPTY") {
      gameTableItems[column][tLine + 2].setTeam(enemyT);
    } else {
      if (teamR == "BLACK") {
        gameTableItems[outColW][outRowW].setTeam("OUTW");
        outRowW++;
      } else {
        gameTableItems[outColB][outRowB].setTeam("OUTB");
        outRowB++;
      }
    }
  }

  gameTableItems[pos[itemsInPos - 1][0]][pos[itemsInPos - 1][1]].setTeam(
    "EMPTY"
  );
  changeTeam();
}

function moveBPushL(column, tLine) {
  gameTableItems[column][tLine].setTeam(teamR);

  if (gameTableItems[column][tLine - 1].team == "EMPTY") {
    gameTableItems[column][tLine - 1].setTeam(enemyT);
  } else if (
    gameTableItems[column][tLine - 1].team == "OUT" ||
    gameTableItems[column][tLine - 1].team == "OUTW" ||
    gameTableItems[column][tLine - 1].team == "OUTB"
  ) {
    if (teamR == "BLACK") {
      gameTableItems[outColW][outRowW].setTeam("OUTW");
      outRowW++;
    } else {
      gameTableItems[outColB][outRowB].setTeam("OUTB");
      outRowB++;
    }
  } else if (gameTableItems[column][tLine - 1].team == enemyT) {
    if (gameTableItems[column][tLine - 2].team == "EMPTY") {
      gameTableItems[column][tLine - 2].setTeam(enemyT);
    } else {
      if (teamR == "BLACK") {
        gameTableItems[outColW][outRowW].setTeam("OUTW");
        outRowW++;
      } else {
        gameTableItems[outColB][outRowB].setTeam("OUTB");
        outRowB++;
      }
    }
  }

  gameTableItems[pos[itemsInPos - 1][0]][pos[itemsInPos - 1][1]].setTeam(
    "EMPTY"
  );
  changeTeam();
}

function moveBPushLRU(column, tLine) {
  gameTableItems[column][tLine].setTeam(teamR);

  if (gameTableItems[column - 1][tLine + 1].team == "EMPTY") {
    gameTableItems[column - 1][tLine + 1].setTeam(enemyT);
  } else if (
    gameTableItems[column - 1][tLine + 1].team == "OUT" ||
    gameTableItems[column - 1][tLine + 1].team == "OUTW" ||
    gameTableItems[column - 1][tLine + 1].team == "OUTB"
  ) {
    if (teamR == "BLACK") {
      gameTableItems[outColW][outRowW].setTeam("OUTW");
      outRowW++;
    } else {
      gameTableItems[outColB][outRowB].setTeam("OUTB");
      outRowB++;
    }
  } else if (gameTableItems[column - 1][tLine + 1].team == enemyT) {
    if (gameTableItems[column - 2][tLine + 2].team == "EMPTY") {
      gameTableItems[column - 2][tLine + 2].setTeam(enemyT);
    } else {
      if (teamR == "BLACK") {
        gameTableItems[outColW][outRowW].setTeam("OUTW");
        outRowW++;
      } else {
        gameTableItems[outColB][outRowB].setTeam("OUTB");
        outRowB++;
      }
    }
  }

  gameTableItems[pos[itemsInPos - 1][0]][pos[itemsInPos - 1][1]].setTeam(
    "EMPTY"
  );
  changeTeam();
}

function moveBPushRLU(column, tLine) {
  gameTableItems[column][tLine].setTeam(teamR);

  if (gameTableItems[column - 1][tLine].team == "EMPTY") {
    gameTableItems[column - 1][tLine].setTeam(enemyT);
  } else if (
    gameTableItems[column - 1][tLine].team == "OUT" ||
    gameTableItems[column - 1][tLine].team == "OUTW" ||
    gameTableItems[column - 1][tLine].team == "OUTB"
  ) {
    if (teamR == "BLACK") {
      gameTableItems[outColW][outRowW].setTeam("OUTW");
      outRowW++;
    } else {
      gameTableItems[outColB][outRowB].setTeam("OUTB");
      outRowB++;
    }
  } else if (gameTableItems[column - 1][tLine].team == enemyT) {
    if (gameTableItems[column - 2][tLine].team == "EMPTY") {
      gameTableItems[column - 2][tLine].setTeam(enemyT);
    } else {
      if (teamR == "BLACK") {
        gameTableItems[outColW][outRowW].setTeam("OUTW");
        outRowW++;
      } else {
        gameTableItems[outColB][outRowB].setTeam("OUTB");
        outRowB++;
      }
    }
  }

  gameTableItems[pos[itemsInPos - 1][0]][pos[itemsInPos - 1][1]].setTeam(
    "EMPTY"
  );
  changeTeam();
}

function moveBMultipleLeft2(column, tLine) {
  gameTableItems[column][tLine].setTeam(teamR);
  gameTableItems[column][tLine + 1].setTeam(teamR);
  gameTableItems[pos[itemsInPos - 1][0]][pos[itemsInPos - 1][1]].setTeam(
    "EMPTY"
  );
  gameTableItems[pos[itemsInPos - 1][0]][pos[itemsInPos - 1][1] - 1].setTeam(
    "EMPTY"
  );
  changeTeam();
}

function moveBMultipleLeft3(column, tLine) {
  gameTableItems[column][tLine].setTeam(teamR);
  gameTableItems[column][tLine + 1].setTeam(teamR);
  gameTableItems[column][tLine + 2].setTeam(teamR);
  gameTableItems[pos[itemsInPos - 1][0]][pos[itemsInPos - 1][1]].setTeam(
    "EMPTY"
  );
  gameTableItems[pos[itemsInPos - 1][0]][pos[itemsInPos - 1][1] - 1].setTeam(
    "EMPTY"
  );
  gameTableItems[pos[itemsInPos - 1][0]][pos[itemsInPos - 1][1] - 2].setTeam(
    "EMPTY"
  );
  changeTeam();
}

function moveBMultipleDiagonalUp2(column, tLine) {
  gameTableItems[column][tLine].setTeam(teamR);
  gameTableItems[column + 1][tLine - 1].setTeam(teamR);
  gameTableItems[pos[itemsInPos - 1][0]][pos[itemsInPos - 1][1]].setTeam(
    "EMPTY"
  );
  gameTableItems[pos[itemsInPos - 1][0] - 1][
    pos[itemsInPos - 1][1] + 1
  ].setTeam("EMPTY");
  changeTeam();
}

function moveBMultipleDiagonalUp2RL(column, tLine) {
  gameTableItems[column][tLine].setTeam(teamR);
  gameTableItems[column + 1][tLine].setTeam(teamR);
  gameTableItems[pos[itemsInPos - 1][0]][pos[itemsInPos - 1][1]].setTeam(
    "EMPTY"
  );
  gameTableItems[pos[itemsInPos - 1][0] - 1][pos[itemsInPos - 1][1]].setTeam(
    "EMPTY"
  );
  changeTeam();
}

function moveBMultipleDiagonalLeftUp3(column, tLine) {
  gameTableItems[column][tLine].setTeam(teamR);
  gameTableItems[column + 1][tLine - 1].setTeam(teamR);
  gameTableItems[column + 2][tLine - 2].setTeam(teamR);
  gameTableItems[pos[itemsInPos - 1][0]][pos[itemsInPos - 1][1]].setTeam(
    "EMPTY"
  );
  gameTableItems[pos[itemsInPos - 1][0] - 1][
    pos[itemsInPos - 1][1] + 1
  ].setTeam("EMPTY");
  gameTableItems[pos[itemsInPos - 1][0] - 2][
    pos[itemsInPos - 1][1] + 2
  ].setTeam("EMPTY");
  changeTeam();
}

function moveBMultipleDiagonalLeftUp3RL(column, tLine) {
  gameTableItems[column][tLine].setTeam(teamR);
  gameTableItems[column + 1][tLine].setTeam(teamR);
  gameTableItems[column + 2][tLine].setTeam(teamR);
  gameTableItems[pos[itemsInPos - 1][0]][pos[itemsInPos - 1][1]].setTeam(
    "EMPTY"
  );
  gameTableItems[pos[itemsInPos - 1][0] - 1][pos[itemsInPos - 1][1]].setTeam(
    "EMPTY"
  );
  gameTableItems[pos[itemsInPos - 1][0] - 2][pos[itemsInPos - 1][1]].setTeam(
    "EMPTY"
  );
  changeTeam();
}

function moveBMultipleDiagonalDown2(column, tLine) {
  gameTableItems[column][tLine].setTeam(teamR);
  gameTableItems[column - 1][tLine + 1].setTeam(teamR);
  gameTableItems[pos[itemsInPos - 1][0]][pos[itemsInPos - 1][1]].setTeam(
    "EMPTY"
  );
  gameTableItems[pos[itemsInPos - 1][0] + 1][
    pos[itemsInPos - 1][1] - 1
  ].setTeam("EMPTY");
  changeTeam();
}

function moveBMultipleDiagonalDown2RL(column, tLine) {
  gameTableItems[column][tLine].setTeam(teamR);
  gameTableItems[column - 1][tLine].setTeam(teamR);
  gameTableItems[pos[itemsInPos - 1][0]][pos[itemsInPos - 1][1]].setTeam(
    "EMPTY"
  );
  gameTableItems[pos[itemsInPos - 1][0] + 1][pos[itemsInPos - 1][1]].setTeam(
    "EMPTY"
  );
  changeTeam();
}

function moveBMultipleDiagonalLeftDown3(column, tLine) {
  gameTableItems[column][tLine].setTeam(teamR);
  gameTableItems[column - 1][tLine + 1].setTeam(teamR);
  gameTableItems[column - 2][tLine + 2].setTeam(teamR);
  gameTableItems[pos[itemsInPos - 1][0]][pos[itemsInPos - 1][1]].setTeam(
    "EMPTY"
  );
  gameTableItems[pos[itemsInPos - 1][0] + 1][
    pos[itemsInPos - 1][1] - 1
  ].setTeam("EMPTY");
  gameTableItems[pos[itemsInPos - 1][0] + 2][
    pos[itemsInPos - 1][1] - 2
  ].setTeam("EMPTY");
  changeTeam();
}

function moveBMultipleDiagonalLeftDown3RL(column, tLine) {
  gameTableItems[column][tLine].setTeam(teamR);
  gameTableItems[column - 1][tLine].setTeam(teamR);
  gameTableItems[column - 2][tLine].setTeam(teamR);
  gameTableItems[pos[itemsInPos - 1][0]][pos[itemsInPos - 1][1]].setTeam(
    "EMPTY"
  );
  gameTableItems[pos[itemsInPos - 1][0] + 1][pos[itemsInPos - 1][1]].setTeam(
    "EMPTY"
  );
  gameTableItems[pos[itemsInPos - 1][0] + 2][pos[itemsInPos - 1][1]].setTeam(
    "EMPTY"
  );
  changeTeam();
}

function moveBMultipleDiagonalRigthDown2(column, tLine) {
  gameTableItems[column][tLine].setTeam(teamR);
  gameTableItems[column - 1][tLine + 1].setTeam(teamR);
  gameTableItems[pos[itemsInPos - 1][0]][pos[itemsInPos - 1][1]].setTeam(
    "EMPTY"
  );
  gameTableItems[pos[itemsInPos - 1][0] + 1][
    pos[itemsInPos - 1][1] - 1
  ].setTeam("EMPTY");
  changeTeam();
}

function moveBMultipleRigth2(column, tLine) {
  gameTableItems[column][tLine].setTeam(teamR);
  gameTableItems[column][tLine - 1].setTeam(teamR);
  gameTableItems[pos[itemsInPos - 1][0]][pos[itemsInPos - 1][1]].setTeam(
    "EMPTY"
  );
  gameTableItems[pos[itemsInPos - 1][0]][pos[itemsInPos - 1][1] + 1].setTeam(
    "EMPTY"
  );
  changeTeam();
}

function moveBMultipleRigth3(column, tLine) {
  gameTableItems[column][tLine].setTeam(teamR);
  gameTableItems[column][tLine - 1].setTeam(teamR);
  gameTableItems[column][tLine - 2].setTeam(teamR);
  gameTableItems[pos[itemsInPos - 1][0]][pos[itemsInPos - 1][1]].setTeam(
    "EMPTY"
  );
  gameTableItems[pos[itemsInPos - 1][0]][pos[itemsInPos - 1][1] + 1].setTeam(
    "EMPTY"
  );
  gameTableItems[pos[itemsInPos - 1][0]][pos[itemsInPos - 1][1] + 2].setTeam(
    "EMPTY"
  );
  changeTeam();
}

function changeTeam() {
  if (teamR == "BLACK") {
    teamR = "WHITE";
  } else {
    teamR = "BLACK";
  }
}

function resetAll() {
  defaultTeamPos();
  rename();
  startBtn.show();
  stopBot();
  play = false;
}

function gameTableBG() {
  // game table
  fill(150);
  if (radius > 35) {
    size1 = radius * 7;
    size2 = radius * 5.5;
    posX = canvasWidth / 2;
    posY = canvasHeight / 2;
  } else {
    size1 = radius + radius * 6;
    size2 = radius + radius * 5;
    posX = canvasWidth / 2 + 15;
    posY = canvasHeight / 2;
  }
  polygon(posX, posY, size1, 6);
  polygon(posX, posY, size2, 6);
}

function gameTable() {
  var distanceX = radius + 10;
  var distanceY = radius + 5;
  var xDefault = canvasWidth / 2 - distanceX * 3 - radius * 5 - paddingTb;
  var x = xDefault - (radius / 2 + 5);
  var y = canvasHeight / 2 - distanceY * 5;
  var length = 11;
  var columnL = 11;
  for (let column = 0; column < columnL; column++) {
    gameTableItems[column] = new Array();
    for (let tLine = 0; tLine < length; tLine++) {
      gameTableItems[column].push(new gamePiece(x, y, radius));
      x += distanceX;
    }
    y += distanceY;
    x = xDefault + column * (radius / 2 + 5);
  }
}

class gamePiece {
  constructor(x, y, radius) {
    this.x = x;
    this.y = y;
    this.radius = radius;
    this.pos = false;
    this.color = "gray";
    this.team = "EMPTY";
    circle(this.x, this.y, this.radius);
  }

  drawPiece() {
    if (this.team == "SELECTED") {
      fill(this.color);
      circle(this.x, this.y, this.radius + 10);
      imageMode(CENTER);
      if (teamR == "WHITE") {
        image(
          whiteSphere,
          this.x,
          this.y,
          this.radius * 1.4,
          this.radius * 1.1
        );
      } else {
        fill("black");
        circle(this.x, this.y, this.radius);
        image(blackSphere, this.x, this.y, this.radius, this.radius);
      }
    }
    if (this.team != "NO" && this.team != "SELECTED") {
      fill(this.color);
      circle(this.x, this.y, this.radius);
    }
  }

  checkColor() {
    if (this.team == "BLACK") {
      this.color = "black";
      imageMode(CENTER);
      image(blackSphere, this.x, this.y, this.radius, this.radius);
    } else if (this.team == "WHITE") {
      this.color = "white";
      imageMode(CENTER);
      image(whiteSphere, this.x, this.y, this.radius * 1.4, this.radius * 1.1);
    } else if (this.team == "EMPTY") {
      this.color = "gray";
    } else if (this.team == "OUT") {
      this.color = "#686868";
    } else if (this.team == "SELECTED") {
      this.color = "orange";
    } else if (this.team == "OUTW") {
      this.color = "white";
    } else if (this.team == "OUTB") {
      this.color = "black";
    } else if (
      this.team == "MoveL" ||
      this.team == "MoveR" ||
      this.team == "MoveLRD" ||
      this.team == "MoveRLD" ||
      this.team == "MoveLRU" ||
      this.team == "MoveRLU"
    ) {
      if (teamR == "BLACK") {
        this.color == "white";
      } else {
        this.color = "black";
      }
    } else {
      this.color = "#505050";
    }
  }

  setTeam(team) {
    this.team = team;
  }
}

function polygon(x, y, radius, npoints) {
  let angle = TWO_PI / npoints;
  beginShape();
  for (let a = 0; a < TWO_PI; a += angle) {
    let sx = x + cos(a) * radius;
    let sy = y + sin(a) * radius;
    vertex(sx, sy);
  }
  endShape(CLOSE);
}

function delExtraCircle() {
  //redefine extra circles
  for (let i = 5; i < gameTableItems[0].length - 1; i++) {
    gameTableItems[0][i].setTeam("OUT");
  }
  for (let i = 0; i <= 5; i++) {
    gameTableItems[10][i].setTeam("OUT");
  }

  for (let i = 0; i < gameTableItems.length; i++) {
    if (i < gameTableItems.length / 2) {
      gameTableItems[i][10].setTeam("OUT");
    }
    if (i > gameTableItems.length / 2 - 1) {
      gameTableItems[i][0].setTeam("OUT");
    } else {
      gameTableItems[i][0].setTeam("NO");
    }
  }
  let delLeng = 5;
  for (let i = 0; i < 5; i++) {
    for (let j = 1; j <= delLeng; j++) {
      if (j == delLeng) {
        gameTableItems[i][j].setTeam("OUT");
      } else {
        gameTableItems[i][j].setTeam("NO");
      }
    }
    delLeng--;
  }
  delLeng = 9;
  for (let i = 6; i < 11; i++) {
    for (let j = 10; j >= delLeng; j--) {
      if (j == delLeng) {
        gameTableItems[i][j].setTeam("OUT");
      } else {
        gameTableItems[i][j].setTeam("NO");
      }
    }
    delLeng--;
  }
}

function defaultTeamPos() {
  delExtraCircle();
  outColW = 10;
  outRowW = 0;
  outColB = 0;
  outRowB = 5;
  teamR = "WHITE";
  for (let column = 0; column < gameTableItems.length; column++) {
    for (let tLine = 0; tLine < gameTableItems[column].length; tLine++) {
      if (
        gameTableItems[column][tLine].team != "NO" &&
        gameTableItems[column][tLine].team != "OUT"
      )
        gameTableItems[column][tLine].setTeam("EMPTY");
    }
  }

  pos = [];
  nrOfPieces = 11;
  // nrOfPieces = 0;
  for (let column = 0; column < gameTableItems.length / 2 - 1; column++) {
    for (let tLine = 0; tLine < gameTableItems[column].length; tLine++) {
      if (
        nrOfPieces > 0 &&
        column != 12 &&
        column != 11 &&
        gameTableItems[column][tLine].team == "EMPTY"
      ) {
        gameTableItems[column][tLine].setTeam("BLACK");
        nrOfPieces--;
      }
    }
  }

  gameTableItems[3][5].setTeam("BLACK");
  gameTableItems[3][6].setTeam("BLACK");
  gameTableItems[3][7].setTeam("BLACK");

  nrOfPieces = 11;
  for (let column = gameTableItems.length - 1; column >= 0; column--) {
    for (let tLine = 0; tLine < gameTableItems[column].length; tLine++) {
      if (
        nrOfPieces > 0 &&
        column != 12 &&
        column != 11 &&
        gameTableItems[column][tLine].team == "EMPTY"
      ) {
        gameTableItems[column][tLine].setTeam("WHITE");
        nrOfPieces--;
      }
    }
  }
  gameTableItems[7][3].setTeam("WHITE");
  gameTableItems[7][4].setTeam("WHITE");
  gameTableItems[7][5].setTeam("WHITE");
}

function resetAllDif() {
  resetAllSelected();
  resetMove();
}
