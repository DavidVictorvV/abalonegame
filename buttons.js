var pName1 = "";
var pName2 = "";
var inpP1, inpP2;

function addPNameP1() {
  let pName = inpP1.value();
  pName1 = pName;
  if (pName1 == "BOT") {
    botTeam = "WHITE";
  }
  inpP1.hide();
  P1button.hide();
}
function addPNameP2() {
  let pName = inpP2.value();
  pName2 = pName;
  if (pName2 == "BOT") {
    botTeam = "BLACK";
  }
  lP2 = pName2.length;
  inpP2.hide();
  P2button.hide();
}

function rename() {
  botTeam = "";
  inpP1.value("");
  inpP1.show();
  pName1 = "";
  P1button.show();
  inpP2.value("");
  inpP2.show();
  pName2 = "";
  P2button.show();
}
function resetButton() {
  resetBtn = createButton("Reset");
  resetBtn.mousePressed(resetAll);
  resetBtn.position(canvasWidth / 2 - 25, 1);
}

function playerNamesInput() {
  inpP1 = createInput("");
  inpP1.position(
    gameTableItems[0][5].x - gameTableItems[0][5].x / 9,
    gameTableItems[0][5].y - gameTableItems[0][5].y / 1.45 - 5
  );
  inpP1.size(100);
  inpP2 = createInput("");
  inpP2.position(
    gameTableItems[0][9].x - gameTableItems[0][9].x / 50,
    gameTableItems[0][9].y - gameTableItems[0][9].y / 1.45 - 5
  );
  inpP2.size(100);
  P1button = createButton("Submit");
  P1button.mousePressed(addPNameP1);
  P1button.position(
    gameTableItems[0][5].x - gameTableItems[0][5].x / 9 + 25,
    gameTableItems[0][5].y - gameTableItems[0][5].y / 1.7
  );
  P2button = createButton("Submit");
  P2button.mousePressed(addPNameP2);
  P2button.position(
    gameTableItems[0][9].x - gameTableItems[0][5].x / 50 + 20,
    gameTableItems[0][9].y - gameTableItems[0][5].y / 1.7
  );

  reNameBtn = createButton("Rename");
  reNameBtn.mousePressed(rename);
  reNameBtn.position(10, 10);
}

function playerNames() {
  textSize(25);
  fill("white");

  text(
    pName1,
    gameTableItems[0][7].x - pName1.length * 10,
    gameTableItems[0][5].y - gameTableItems[0][5].y / 1.6
  );
  if (pName1 == "") {
    text(
      "P1: ",
      gameTableItems[0][5].x - gameTableItems[0][5].x / 9 - 40,
      gameTableItems[0][5].y - gameTableItems[0][5].y / 1.6
    );
  }
  fill("black");
  text(
    pName2,
    gameTableItems[0][8].x + pName1.length * 10,
    gameTableItems[0][9].y - gameTableItems[0][9].y / 1.6
  );
  if (pName2 == "") {
    text(
      "P2: ",
      gameTableItems[0][9].x - gameTableItems[0][9].x / 50 - 40,
      gameTableItems[0][9].y - gameTableItems[0][9].y / 1.6
    );
  }
  fill(teamR);
  if (teamR == "WHITE") {
    textSize(25);

    text(
      "<",
      canvasWidth / 2,
      gameTableItems[0][5].y - gameTableItems[0][5].y / 1.6
    );
  } else {
    textSize(27);
    text(
      ">",
      canvasWidth / 2,
      gameTableItems[0][5].y - gameTableItems[0][5].y / 1.6
    );
  }
}
