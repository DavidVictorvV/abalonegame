//options(bot vs bot)
var optionShow = false;
var botSpeed = 3000;
var moveInterval;
var moveInterval2;
var bCheck;
var randomColSel;
var randomRowSel;
var randomXMove = 0;
var randomYMove = 0;
var botTeam;
var playingBvB = false;
function startBot() {
  botPosibleMoves();
  botMove();
}

function botVbot() {
  if (play) {
    playingBvB = true;
    botvBotStartMoveBtn.style("background-color", "lightgray");
    botvBotStopMoveBtn.style("background-color", "white");
    stopBot();
    moveInterval = setInterval(() => {
      botPosibleMoves();
    }, botSpeed);
    moveInterval2 = setInterval(() => {
      botMove();
    }, botSpeed);
  }
}

function stopButton() {
  if (play && playingBvB) {
    playingBvB = false;
    stopBot();
    botvBotStartMoveBtn.style("background-color", "white");
    botvBotStopMoveBtn.style("background-color", "ligthgray");
  }
}

function stopBot() {
  clearInterval(moveInterval);
  clearInterval(moveInterval2);
}

function botSpeed1() {
  botSpeed = 1000;
  botVbosSpeed1.style("background-color", "red");
  botVbosSpeed2.style("background-color", "white");
  botVbosSpeed3.style("background-color", "white");
  if (play && playingBvB) {
    stopBot();
    botVbot();
  }
}
function botSpeed2() {
  botSpeed = 100;
  botVbosSpeed1.style("background-color", "white");
  botVbosSpeed2.style("background-color", "red");
  botVbosSpeed3.style("background-color", "white");
  if (play && playingBvB) {
    stopBot();
    botVbot();
  }
}
function botSpeed3() {
  botSpeed = 30;
  botVbosSpeed1.style("background-color", "white");
  botVbosSpeed2.style("background-color", "white");
  botVbosSpeed3.style("background-color", "red");
  if (play && playingBvB) {
    stopBot();
    botVbot();
  }
}

function optionToggle() {
  if (optionShow == false) {
    optionShow = true;
    botvBotStartMoveBtn.show();
    botvBotStopMoveBtn.show();
    botVbosSpeed1.show();
    botVbosSpeed2.show();
    botVbosSpeed3.show();
  } else {
    optionShow = false;
    botvBotStartMoveBtn.hide();
    botvBotStopMoveBtn.hide();
    botVbosSpeed1.hide();
    botVbosSpeed2.hide();
    botVbosSpeed3.hide();
  }
}

function botButtons() {
  optionsBtn = createButton("<");
  optionsBtn.position(canvasWidth - 30, 5);
  optionsBtn.mousePressed(optionToggle);

  botvBotStartMoveBtn = createButton("Start");
  botvBotStartMoveBtn.position(canvasWidth - 80, 25);
  botvBotStartMoveBtn.mousePressed(botVbot);
  botvBotStartMoveBtn.size(50);
  botvBotStartMoveBtn.hide();

  botvBotStopMoveBtn = createButton("Stop");
  botvBotStopMoveBtn.position(canvasWidth - 80, 45);
  botvBotStopMoveBtn.mousePressed(stopButton);
  botvBotStopMoveBtn.size(50);
  botvBotStopMoveBtn.hide();

  botVbosSpeed1 = createButton("1");
  botVbosSpeed1.position(canvasWidth - 80, 5);
  botVbosSpeed1.mousePressed(botSpeed1);
  botVbosSpeed1.size(1);
  botVbosSpeed1.hide();

  botVbosSpeed2 = createButton("2");
  botVbosSpeed2.position(canvasWidth - 64, 5);
  botVbosSpeed2.mousePressed(botSpeed2);
  botVbosSpeed2.size(1);
  botVbosSpeed2.hide();

  botVbosSpeed3 = createButton("3");
  botVbosSpeed3.position(canvasWidth - 48, 5);
  botVbosSpeed3.mousePressed(botSpeed3);
  botVbosSpeed3.size(1);
  botVbosSpeed3.hide();
}

function checkForPos(column, tLine) {
  if (randomColSel == column && randomRowSel == tLine) {
    return true;
  }
  return false;
}

function botPosibleMoves() {
  do {
    randomColSel = floor(random(0, 10));
    randomRowSel = floor(random(0, 10));
  } while (gameTableItems[randomColSel][randomRowSel].team != teamR);
  for (let column = 0; column < gameTableItems.length - 1; column++) {
    for (let tLine = 0; tLine < gameTableItems[column].length - 1; tLine++) {
      if (
        checkForPos(column, tLine) &&
        gameTableItems[column][tLine].team == teamR
      ) {
        pos = [];
        itemsInPos = 0;
        pos[itemsInPos] = new Array();
        pos[itemsInPos].push(column);
        pos[itemsInPos].push(tLine);
        itemsInPos++;
        selectPiece();
      }
    }
  }
}

function checkForMove(column, tLine) {
  if (randomXMove == column && randomYMove == tLine) {
    return true;
  }
  return false;
}

function botMove() {
  let move = floor(random(0, movablePos.length));
  let column = movablePos[move].col;
  let tLine = movablePos[move].row;
  if (
    gameTableItems[column][tLine].team == "MOVE1" ||
    gameTableItems[column][tLine].team == "MoveLR"
  ) {
    moveB(column, tLine);
    resetAllDif();
  }

  if (gameTableItems[column][tLine].team == "MoveLeftMultiple2") {
    moveBMultipleLeft2(column, tLine);
    resetAllDif();
  }
  if (gameTableItems[column][tLine].team == "MoveRigthMultiple2") {
    moveBMultipleRigth2(column, tLine);
    resetAllDif();
  }
  if (gameTableItems[column][tLine].team == "MoveLeftMultiple3") {
    moveBMultipleLeft3(column, tLine);
    resetAllDif();
  }
  if (gameTableItems[column][tLine].team == "MoveRigthMultiple3") {
    moveBMultipleRigth3(column, tLine);
    resetAllDif();
  }
  if (
    gameTableItems[column][tLine].team == "MoveDiagonalLR-LU2" ||
    gameTableItems[column][tLine].team == "MoveDiagonalLR-RU2"
  ) {
    moveBMultipleDiagonalUp2(column, tLine);
    resetAllDif();
  }
  if (
    gameTableItems[column][tLine].team == "MoveDiagonalLR-LD2" ||
    gameTableItems[column][tLine].team == "MoveDiagonalLR-RD2"
  ) {
    moveBMultipleDiagonalDown2(column, tLine);
    resetAllDif();
  }
  if (
    gameTableItems[column][tLine].team == "MoveDiagonalLR-LU3" ||
    gameTableItems[column][tLine].team == "MoveDiagonalLR-RU3"
  ) {
    moveBMultipleDiagonalLeftUp3(column, tLine);
    resetAllDif();
  }
  if (
    gameTableItems[column][tLine].team == "MoveDiagonalLR-LD3" ||
    gameTableItems[column][tLine].team == "MoveDiagonalLR-RD3"
  ) {
    moveBMultipleDiagonalLeftDown3(column, tLine);
    resetAllDif();
  }
  if (
    gameTableItems[column][tLine].team == "MoveDiagonalRL-LU2" ||
    gameTableItems[column][tLine].team == "MoveDiagonalRL-RU2"
  ) {
    moveBMultipleDiagonalUp2RL(column, tLine);
    resetAllDif();
  }
  if (
    gameTableItems[column][tLine].team == "MoveDiagonalRL-LD2" ||
    gameTableItems[column][tLine].team == "MoveDiagonalRL-RD2"
  ) {
    moveBMultipleDiagonalDown2RL(column, tLine);
    resetAllDif();
  }
  if (
    gameTableItems[column][tLine].team == "MoveDiagonalRL-LU3" ||
    gameTableItems[column][tLine].team == "MoveDiagonalRL-RU3"
  ) {
    moveBMultipleDiagonalLeftUp3RL(column, tLine);
    resetAllDif();
  }
  if (
    gameTableItems[column][tLine].team == "MoveDiagonalRL-LD3" ||
    gameTableItems[column][tLine].team == "MoveDiagonalRL-RD3"
  ) {
    moveBMultipleDiagonalLeftDown3RL(column, tLine);
    resetAllDif();
  }
  if (gameTableItems[column][tLine].team == "MoveRLD") {
    moveBPushRLD(column, tLine);
    resetAllDif();
  }
  if (gameTableItems[column][tLine].team == "MoveLRU") {
    moveBPushLRU(column, tLine);
    resetAllDif();
  }
  if (gameTableItems[column][tLine].team == "MoveRLU") {
    moveBPushRLU(column, tLine);
    resetAllDif();
  }
  if (gameTableItems[column][tLine].team == "MoveLRD") {
    moveBPushLRD(column, tLine);
    resetAllDif();
  }
  if (gameTableItems[column][tLine].team == "MoveR") {
    moveBPushR(column, tLine);
    resetAllDif();
  }
  if (gameTableItems[column][tLine].team == "MoveL") {
    moveBPushL(column, tLine);
    resetAllDif();
  }
}
